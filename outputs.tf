output "tf_key_id" {
  description = "The id of the KMS key used to encrypt the S3 bucket"
  value       = aws_kms_key.terraform_key.key_id
}

output "tf_s3_bucket" {
  description = "The name of the global S3 bucket housing the set of TF state files"
  value = (
    length(aws_s3_bucket.tf_state_bucket_sturdy[*]) > 0
    ? aws_s3_bucket.tf_state_bucket_sturdy[0].id
    : aws_s3_bucket.tf_state_bucket_fragile[0].id
  )
}

output "tf_db_table" {
  description = "The name of the DynamoDB table married to the S3 bucket"
  value       = aws_dynamodb_table.terraform_db_locks.name
}
