variable "environment" {
  description = "The environment (dev, stage, prod) to deploy resource(s) to"
  default     = "dev"
  type        = string
}

variable "suffix" {
  description = "Random string to append to resource names to uniquely namespace them"
  default     = ""
  type        = string
}

variable "enableFragility" {
  description = "Toggle both prevent_destroy AND versioning on/off for the S3 bucket"
  default     = false
  type        = bool
}
