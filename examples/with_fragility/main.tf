provider "aws" {
  region = "us-east-1"
}

# Testing module's input variables
# Accept end user's environment value
variable "environment" {
  type = string
}

# Accept end user's suffix value
variable "suffix" {
  default = ""
  type    = string
}

# Accept end user's enableLifecycle value
variable "enableFragility" {
  default = true
  type    = bool
}

# Pass end user's var to module
module "tf_state_bootstrap" {
  source = "../../"

  environment     = var.environment
  suffix          = var.suffix
  enableFragility = var.enableFragility
}

# Pass module's outputs thru this root module's output
output "tf_s3_bucket" {
  description = "The name of the global S3 bucket housing the set of TF state files per environment"
  value       = module.tf_state_bootstrap.tf_s3_bucket
}

output "tf_db_table" {
  description = "The name of the DynamoDB table married to the S3 bucket"
  value       = module.tf_state_bootstrap.tf_db_table
}
