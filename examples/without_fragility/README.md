# Basic Example
This example imports this repo's module.

* git clone this repo and cd into examples/basic
* `terraform apply -var 'environment=dev'` to create the S3 bucket and DynamoDB table
* If you include an extra var in the above command `'enableLifecycle=false'`, it will allow you to more easily destroy the S3 bucket with `terraform destroy`
   * The prevent_destroy lifecycle safeguard prohibits normal `terrform destroy` commands from successfully deleting resources (the S3 bucket) to prevent accidental deletion
   * You can go behind Terraform's back and delete the S3 bucket if you created it with the prevent_destroy enabled (default) through the cmdline API (not a generally advisable practice)
      * aws s3 rb s3://`` `terraform output tf_s3_bucket` ``

      * `terraform destroy -var 'environment=dev'`
