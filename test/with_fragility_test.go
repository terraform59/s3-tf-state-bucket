package test

import (
	"fmt"
	"strings"
	"time"
	"testing"

	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

// Testing the Terraform module in examples/terraform-basic-example with S3 fragile settings
func TestWithFragility(t *testing.T) {
	t.Parallel()

	expectedSuffix := "-" + strings.ToLower(random.UniqueId())
	expectedEnvironment := "test"
	expectedBucketName := fmt.Sprintf("bradthebuilder-%s-tf-state-bucket%s", expectedEnvironment, expectedSuffix)
	expectedTableName := fmt.Sprintf("bradthebuilder-%s-tf-lock%s", expectedEnvironment, expectedSuffix)

	awsRegion := "us-east-1"

	terraformOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "../examples/with_fragility",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"environment": expectedEnvironment,
			"suffix": expectedSuffix,
			"enableFragility": true,
		},

		// Environment variables to set when running Terraform
		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
		},

		// Disable colors in Terraform commands so its easier to parse stdout/stderr
		NoColor: true,
		// Enable locking to prevent state corruption during parallel testing
		Lock: true,
		LockTimeout: "30s",
		// Enable retries
		MaxRetries: 3,
		TimeBetweenRetries: 5 * time.Second,
		RetryableTerraformErrors: map[string]string{
			"RequestError: send request failed" : "DNS hiccup?",
		},
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)
	terraform.InitAndApply(t, terraformOptions)

	bucketID := terraform.Output(t, terraformOptions, "tf_s3_bucket")
	tableID := terraform.Output(t, terraformOptions, "tf_db_table")

	// Assert that bucket exists
	aws.AssertS3BucketExists(t, awsRegion, bucketID)

	// Assert that name of bucket and table are as expected
	fmt.Printf("Bucket name is: %s\n", bucketID)
	assert.Equal(t, expectedBucketName, bucketID)
	assert.Equal(t, expectedTableName, tableID)
}
