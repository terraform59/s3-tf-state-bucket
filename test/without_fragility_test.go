package test

import (
	"fmt"
	"strings"
	"time"
	"testing"
	"os"
	"os/exec"

	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func RemoveStateFiles() {
	fmt.Printf("Removing local state files used for this test...\n")
	// Manually remove tainted state files
	os.Remove("../examples/without_fragility/terraform.tfstate")
	os.Remove("../examples/without_fragility/terraform.tfstate.backup")
	fmt.Printf("DONE\n")
}

// Testing the Terraform module in examples/terraform-basic-example with S3 sturdy settings
func TestWithoutFragility(t *testing.T) {
	t.Parallel()

	expectedSuffix := "-" + strings.ToLower(random.UniqueId())
	expectedEnvironment := "test"
	expectedBucketName := fmt.Sprintf("bradthebuilder-%s-tf-state-bucket%s", expectedEnvironment, expectedSuffix)
	expectedTableName := fmt.Sprintf("bradthebuilder-%s-tf-lock%s", expectedEnvironment, expectedSuffix)

	awsRegion := "us-east-1"

	terraformOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "../examples/without_fragility",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"environment": expectedEnvironment,
			"suffix": expectedSuffix,
			"enableFragility": false,
		},

		// Environment variables to set when running Terraform
		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
		},

		// Disable colors in Terraform commands so its easier to parse stdout/stderr
		NoColor: true,
		// Enable locking to prevent state corruption during parallel testing
		Lock: true,
		LockTimeout: "30s",
		// Enable retries
		MaxRetries: 3,
		TimeBetweenRetries: 5 * time.Second,
		RetryableTerraformErrors: map[string]string{
			"RequestError: send request failed" : "DNS hiccup?",
		},
	}

	// Destroy Options only target deletion of DynamoDB table because S3 bucket is manually deleted below
	terraformDestroyOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "../examples/without_fragility",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"environment": expectedEnvironment,
			"suffix": expectedSuffix,
			"enableFragility": false,
		},

		// Environment variables to set when running Terraform
		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
		},

		// Disable colors in Terraform commands so its easier to parse stdout/stderr
		NoColor: true,
		// Enable locking to prevent state corruption during parallel testing
		Lock: true,
		LockTimeout: "30s",
		// Enable retries
		MaxRetries: 3,
		TimeBetweenRetries: 30 * time.Second,
		RetryableTerraformErrors: map[string]string{
			"RequestError: send request failed" : "DNS hiccup?",
		},
		Targets: []string{
			"module.tf_state_bootstrap.aws_dynamodb_table.terraform_db_locks",
			"module.tf_state_bootstrap.aws_kms_key.terraform_key",
		},
	}

	// Queue removal of state files to run AFTER terraform has torn down the infrastructure
	defer RemoveStateFiles()

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformDestroyOptions)
	terraform.InitAndApply(t, terraformOptions)

	bucketID := terraform.Output(t, terraformOptions, "tf_s3_bucket")
	tableID := terraform.Output(t, terraformOptions, "tf_db_table")

	// Assert that bucket exists
	aws.AssertS3BucketExists(t, awsRegion, bucketID)

	// Assert that name of bucket and table are as expected
	fmt.Printf("Bucket name is: %s\n", bucketID)
	assert.Equal(t, expectedBucketName, bucketID)
	assert.Equal(t, expectedTableName, tableID)

	// Assert that versioning is enabled
	aws.AssertS3BucketVersioningExists(t, awsRegion, bucketID)

	// Manually remove S3 bucket; tf destroy only removes db table and kms key
	aws.DeleteS3Bucket(t, awsRegion, bucketID)

	// Manually refresh Terraform state to recognize absent S3 bucket
	// Required to stop destroy operation from failing on recognizing
	// the bucket as preventing deletion of kms key
	args := []string{
		"terraform",
		"refresh",
		//	"-var 'environment=test'",
		//  "-var 'enableFragility=false'",
		// exec.Command has difficulty distinguishing tf vars as positional args vs subcommands
		// use envronment variable to set the needed tf vars instead
    }
	os.Setenv("TF_VAR_environment", expectedEnvironment)
	os.Setenv("TF_VAR_suffix", expectedSuffix)
	os.Setenv("TF_VAR_enableFragility", "false")
	cmd := exec.Command(args[0], args[1:]...)
	cmd.Dir = "../examples/without_fragility"
	b, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Printf("--------------------------------------\nManual invocation of 'terraform refresh' failed\n--------------------------------------")
	}
	output := string(b[:])
	fmt.Printf("%s\n", output)
	os.Unsetenv("TF_VAR_environment")
	os.Unsetenv("TF_VAR_suffix")
	os.Unsetenv("TF_VAR_enableFragility")
}
