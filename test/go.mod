module gitlab.com/bradthebuilder/terraform59/bradthebuilder_s3_terraform_bucket

go 1.14

require (
	github.com/gruntwork-io/terratest v0.34.5
	github.com/stretchr/testify v1.7.0
)
