locals {
  bucket_name   = format("bradthebuilder-${var.environment}-tf-state-bucket%s", "${var.suffix}")
  db_table_name = format("bradthebuilder-${var.environment}-tf-lock%s", "${var.suffix}")
}

# S3 bucket to hold tf state files
# This first bucket is sturdy; it has prevent_destroy and versioning enabled
resource "aws_s3_bucket" "tf_state_bucket_sturdy" {
  count  = var.enableFragility ? 0 : 1
  bucket = local.bucket_name

  # Prevent accidental deletion of this critical bucket
  lifecycle {
    prevent_destroy = true
  }

  # Enable versioning for roll-back capability of state files
  versioning {
    enabled = true
  }

  # Encrypt state files in the bucket, because they're cleartext
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = aws_kms_key.terraform_key.arn
      }
    }
  }

  tags = {
    Name        = local.bucket_name
    Environment = var.environment
    Owner       = "Terraform"
  }
}

# This second bucket is fragile; it has prevent_destroy and versioning disabled (used for testing)
resource "aws_s3_bucket" "tf_state_bucket_fragile" {
  count  = var.enableFragility ? 1 : 0
  bucket = local.bucket_name

  # Disable prevent_destroy for testing or other non-prod purposes
  lifecycle {
    prevent_destroy = false
  }

  # Disable versioning to enable testing to teardown bucket easily
  versioning {
    enabled = false
  }

  # Encrypt state files in the bucket, because they're cleartext
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = aws_kms_key.terraform_key.arn
      }
    }
  }

  tags = {
    Name        = local.bucket_name
    Environment = var.environment
    Owner       = "Terraform"
  }
}

# Resource to explicitly block the ability for the bucket to be made public
resource "aws_s3_bucket_public_access_block" "tf-state-bucket-block" {
  bucket = (
    length(aws_s3_bucket.tf_state_bucket_sturdy[*]) > 0
    ? aws_s3_bucket.tf_state_bucket_sturdy[0].id
    : aws_s3_bucket.tf_state_bucket_fragile[0].id
  )

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# DynamoDB table to enable locking on tf state files
resource "aws_dynamodb_table" "terraform_db_locks" {
  name         = local.db_table_name
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = local.db_table_name
    Environment = var.environment
    Owner       = "Terraform"
  }
}
