# TF State Bucket
This Terraform config provides the most fundamental bootstrap resources for Terraform managed infrastructure:

* An S3 Bucket to store TF state files remotely
* A DynamoDB table to provide locking on statefiles

## The S3 Bucket
By default, Terraform stores state files (the single source of truth) on the developer's/sysadmin's workstation where the `terraform` binary is run from. This is insufficient for use cases beyond the most trivial because:

* If it is stored on a single workstation, it makes collaborative workflows using the same state file impossible
* It is a single-point of failure; if that admin goes on vacation or turns off the workstation holding the state file, no one can make changes to the infrastructure
* God help you if that single workstation fries and there were no backup copies of that state file

Storing TF state files in an S3 bucket enables collaboration, provides high availability, etc. Doing so, however, brings new concerns that are addressed via the settings on the bucket.
### S3 Bucket Settings
The settings on an S3 bucket intended to house TF state files ought to:

* Prevent Accidental Deletion: Similar to the local-workstation danger, deleting state files that govern entire blocks of instrastructure is a REALLY bad thing, so safegaurds to prevent deleting the state files are absolutely critical
* Versioning: It is very useful to maintain versions on critical state files, enabling roll-back to previous versions
* Encrypted: State files are clear text and can hold sensitive info like database passwords, so buckets absolutely ought to be encrypted server side

### S3 Bucket Settings Toggle
Because the Prevent_Destroy lifecycle and Versioning mentioned above can prevent automated integration testing, a toggle variable is provided to control these settings. By default, the S3 bucket created is "sturdy" (In other words, has the features enabled). Enabling the fragility toggle disables both settings, making testing easier.

## Dynamodb Table
In a collaborative production work environment, it is possible for multiple developers and even automated entities like CI servers to all attempt to work on a set of terraform configurations. This potential race condition for access to the backing state files can produce errors in tests and development, so a locking mechanism is needed to provide serial access to a given state file to prevent these errors. That mechanism is provided by a DynamoDB table.

### DynamoDB Table Settings
The billing mode is set to PER-REQUEST because it is a much cheaper billing strategy when only a few handfuls of terraform commands are expected to run and query the lock table.

The "LockID" hash-key is the primary key of the DB and its spelling is very important; do not change this, or you'll break the locking capability.
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.terraform_db_locks](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_kms_alias.terraform_key_alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.terraform_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_s3_bucket.tf_state_bucket_fragile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.tf_state_bucket_sturdy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_public_access_block.tf-state-bucket-block](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_enableFragility"></a> [enableFragility](#input\_enableFragility) | Toggle both prevent\_destroy AND versioning on/off for the S3 bucket | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment (dev, stage, prod) to deploy resource(s) to | `string` | `"dev"` | no |
| <a name="input_suffix"></a> [suffix](#input\_suffix) | Random string to append to resource names to uniquely namespace them | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_tf_db_table"></a> [tf\_db\_table](#output\_tf\_db\_table) | The name of the DynamoDB table married to the S3 bucket |
| <a name="output_tf_key_id"></a> [tf\_key\_id](#output\_tf\_key\_id) | The id of the KMS key used to encrypt the S3 bucket |
| <a name="output_tf_s3_bucket"></a> [tf\_s3\_bucket](#output\_tf\_s3\_bucket) | The name of the global S3 bucket housing the set of TF state files |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
