locals {
  key_alias_name = format("alias/bradthebuilder-${var.environment}-terraform-key%s", "${var.suffix}")
}

resource "aws_kms_key" "terraform_key" {
  description             = "KMS key used to encrypt Terraform config bucket"
  deletion_window_in_days = 10
  is_enabled              = true
  key_usage               = "ENCRYPT_DECRYPT"

  tags = {
    Name = "Terraform Key"
  }
}

resource "aws_kms_alias" "terraform_key_alias" {
  name          = local.key_alias_name
  target_key_id = aws_kms_key.terraform_key.key_id
}
